﻿using UnityEngine;

public class JarAnimation : MonoBehaviour
{

    void Start()
    {

        int jars = --LevelManager.Instance.HealthJars;
        if (jars <= 2)
        {
            transform.GetChild(2).GetComponent<Animator>().enabled = true;
        }
        if (jars <= 1)
        {
            transform.GetChild(1).GetComponent<Animator>().enabled = true;
        }
        if (jars <= 0)
        {
            transform.GetChild(0).GetComponent<Animator>().enabled = true;
            //TODO KEY --
        }

        //PlayerPrefs.SetInt("jars", LevelManager.Instance.HealthJars);


    }

}
