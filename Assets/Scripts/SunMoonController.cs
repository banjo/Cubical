﻿using UnityEngine;

public class SunMoonController : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(Vector3.zero, Vector3.forward, 10f * Time.deltaTime);
        transform.LookAt(Vector3.zero);
    }
}
