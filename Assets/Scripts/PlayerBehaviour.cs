﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{

    public float rotationPeriod = 0.3f;
    public float sideLength = 1f;

    public bool isRotate = false;
    public float directionX = 0;
    public float directionZ = 0;

    Vector3 startPos;
    Vector3 lastPos;

    public List<Vector3> walked;


    float rotationTime = 0;
    float radius;
    Quaternion fromRotation;
    Quaternion toRotation;

    public Vector3 padPos;
    public bool autoMove = false;

    public static bool CanPlay = false;
    public static Vector3 checkPos = Vector3.zero;
    public static bool SecondChance = false;

    float x, y;

    void Start()
    {
        walked.Add(new Vector3(startPos.x, 0, startPos.z));
        lastPos = transform.position;
        radius = sideLength * Mathf.Sqrt(2f) / 2f;
    }

    void Update()
    {


        x = 0;
        y = 0;
        // For Standalone
#if UNITY_STANDALONE || UNITY_EDITOR

        // キー入力を拾う。
        if (CanPlay && !isRotate)
        {
            x = Input.GetAxisRaw("Vertical");
            if (x == 0)
            {
                y = Input.GetAxisRaw("Horizontal");
            }
        }
#endif
        // For Android or IOS Swipe Controls

#if UNITY_ANDROID || UNITY_IOS
        //Debug.Log( MiniGestureRecognizer.Swipe);
        if (CanPlay && !isRotate)
        {
            if (SwipeControls.Instance.IsSwiping(SwipeDirection.Left))
                y = -1;
            else if (SwipeControls.Instance.IsSwiping(SwipeDirection.Right))
                y = 1;
            else if (SwipeControls.Instance.IsSwiping(SwipeDirection.Up))
                x = 1;
            else if (SwipeControls.Instance.IsSwiping(SwipeDirection.Down))
                x = -1;
        }
#endif

        if (x != 0 || y != 0)
        {
            if (!CheckBounds(y, x))
            {
                x = 0;
                y = 0;
                isRotate = false;
                directionX = 0;
                directionZ = 0;
                return;
            }
        }


        if ((x != 0 || y != 0) && !isRotate)
        {
            directionX = -y;
            directionZ = x;
            startPos = transform.position;
            lastPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
            fromRotation = transform.rotation;
            transform.Rotate(directionZ * 90, 0, directionX * 90, Space.World);
            toRotation = transform.rotation;
            transform.rotation = fromRotation;
            rotationTime = 0;
            isRotate = true;
        }

    }

    private bool CheckBounds(float x, float y)
    {
        float calcX = Mathf.Round(transform.position.x) + x;
        float calcY = Mathf.Round(transform.position.z) + y;
        bool bounds = calcX < LevelManager.Instance.width &&
                      calcX >= 0f &&
                      calcY < LevelManager.Instance.height &&
                      calcY >= 0f;
        //&&!(Mathf.Round(lastPos.x) == calcX && Mathf.Round(lastPos.z) == calcY);
        return bounds;
    }

    void FixedUpdate()
    {

        if (isRotate)
        {

            rotationTime += Time.fixedDeltaTime;
            float ratio = Mathf.Lerp(0, 1, rotationTime / rotationPeriod);

            float thetaRad = Mathf.Lerp(0, Mathf.PI / 2f, ratio);
            float distanceX = -directionX * radius * (Mathf.Cos(45f * Mathf.Deg2Rad) - Mathf.Cos(45f * Mathf.Deg2Rad + thetaRad));
            float distanceY = radius * (Mathf.Sin(45f * Mathf.Deg2Rad + thetaRad) - Mathf.Sin(45f * Mathf.Deg2Rad));
            float distanceZ = directionZ * radius * (Mathf.Cos(45f * Mathf.Deg2Rad) - Mathf.Cos(45f * Mathf.Deg2Rad + thetaRad));


            transform.position = new Vector3(startPos.x + distanceX, startPos.y + distanceY, startPos.z + distanceZ);

            transform.rotation = Quaternion.Lerp(fromRotation, toRotation, ratio);

            if (ratio == 1)
            {
                isRotate = false;
                directionX = 0;
                directionZ = 0;
                rotationTime = 0;

                transform.position = new Vector3(Mathf.Round(transform.position.x), transform.position.y, Mathf.Round(transform.position.z));
            }
        }
    }
}