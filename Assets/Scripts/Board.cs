﻿using UnityEngine;
using System.Collections.Generic;


public class Board {

    public enum TileType
    {
        StartPoint = 0,
        Path,
        FinishPoint,
        CheckPoint,
        Fire,
        Lava,
        Spears,
        Sand,
        Shark,
        Lightning,
        _typeCount
    }

    public static List<GameObject> path;

    public TileType[,] board;
    public GameObject[,] tiles;

    public Vector2 StartPoint;
    public Vector2 FinishPoint;


    public GameObject PathStart;
    public GameObject PathFinish;


    int width;
    int height;



    // CONSTUCTOR
    public Board(int[][] matrix, GameObject TilePrefab, GameObject ChestPrefab) {
        
        this.width = LevelManager.Instance.width;
        this.height = LevelManager.Instance.height;
        this.board = new TileType[width, height];
        this.tiles = new GameObject[width, height];
        path = new List<GameObject>();

        // CREATE BOARD GAMEOBJECT
        GameObject boardObject = new GameObject();
        boardObject.transform.SetParent(GameObject.FindGameObjectWithTag("Game").transform);
        boardObject.name = "Board";
        boardObject.transform.position = Vector3.zero;



        int k = this.width - 1;
        for (int i = 0; i < this.width; i++)
        {

            for (int j = 0; j < this.height; j++)
            {

                board[i, j] = (TileType)matrix[i][j]; //Random.Range (3, (int)TileType._typeCount - 1);

                // CREATE TILE GAMEOBJECTS
                GameObject tile = GameObject.Instantiate(TilePrefab, new Vector3((float)j, 0, (float)k), Quaternion.identity) as GameObject;
                tile.transform.SetParent(boardObject.transform);
                tile.GetComponent<TileBehaviour>().type = board[i, j];
                tiles[i, j] = tile;

                int currentTile = (int)board[i, j];

                if (currentTile < 0)
                {
                    tiles[i, j].GetComponent<TileBehaviour>().type = (TileType)Random.Range((int)TileType.Fire, (int)TileType._typeCount);
                }

                if (currentTile <= (int)TileType.CheckPoint && currentTile >= (int)TileType.StartPoint)
                {
                    path.Add(tile);
                }

                if (board[i, j] == TileType.StartPoint)
                {
                    this.StartPoint = new Vector2(j, k);
                    tiles[i, j].GetComponent<TileBehaviour>().type = board[i, j];
                    PathStart = tiles[i, j];
                }
                else if (board[i, j] == TileType.FinishPoint)
                {
                    this.FinishPoint = new Vector2(j, k);
                    tiles[i, j].GetComponent<TileBehaviour>().type = board[i, j];
                    SpawnChest(ChestPrefab, j, k);
                    PathFinish = tiles[i, j];
                }
                else if (board[i, j] == TileType.CheckPoint)
                {
                    tiles[i, j].GetComponent<TileBehaviour>().type = board[i, j];
                }

            }
            k--;
        }   

    }


    private void SpawnChest(GameObject chest, int i, int j)
    {
        float x = (float)i;
        float y = (float)j;
        float rot = 0;

        if (i == width - 1)
        {
            x++;
        }
        else if (i == 0)
        {
            x--;
        }
        else if (j == width - 1)
        {
            y++;
            rot = 90;
        }
        else if (j == 0)
        {
            y--;
            rot = 90;
        }
        else
        {
            return;
        }

        GameObject chestObject = GameObject.Instantiate(chest, new Vector3(x, 1f, y), Quaternion.Euler(0, rot, 0)) as GameObject;
        chestObject.name = "Chest";

    }

    /*
        public bool checkBlacklistNeighbors(Queue<Vector2> blacklist) {
            Vector2 currentPos;
            if (this.path.Count > 1) {
                currentPos = (Vector2)path.Peek ();
            } else {
                return false;
            }


            List<bool> posDirs = new List<bool>();
            posDirs.Add(currentPos.y + 1 < height && blacklist.Contains(new Vector2(currentPos.x, currentPos.y + 1)));//up
            posDirs.Add(currentPos.y - 1 >= 0 && blacklist.Contains(new Vector2(currentPos.x, currentPos.y - 1)));//down
            posDirs.Add(currentPos.x - 1 >= 0 && blacklist.Contains(new Vector2(currentPos.x - 1, currentPos.y)));//left
            posDirs.Add(currentPos.x + 1 < width && blacklist.Contains(new Vector2(currentPos.x + 1, currentPos.y)));//right
            return posDirs.Where(e => e == true).Count() > 0;
        }


        public void AlgoBitch(int a, int b){


            int k = 0;
            int c = 0;



            int[,] pos = new int[4, 2]{  { 1, 0 }, { 0, 1 }, { 0, -1 }, { -1, 0 } };


            Queue<Vector2> blacklist = new Queue<Vector2>();

            int posInd = 0;
            while (board [a, b] != TileType.FinishPoint) {

                //int dirs = 0;
                do {
                    //k = Random.Range (-1, 2);
                    //c = Random.Range (-1, 2);
                    k = pos[posInd,0];
                    c = pos[posInd,1];



                    if(!checkNeighboars (a + k, b + c) && path.Count > 1 || checkBlacklistNeighbors(blacklist)){
                        Vector2 pop = (Vector2) path.Pop ();

                        board[(int)pop.x, (int)pop.y] = TileType.Lightning;
                        Vector2 v = (Vector2)path.Peek ();
                        a = (int)v.x;
                        b = (int)v.y;
                        posInd += 1;
                        blacklist.Clear();
                        blacklist.Enqueue(pop);
                    }



                    if(Mathf.Abs(a+k) == Mathf.Abs(b+c)){
                        Debug.Log("BLA");
                    }
                    posInd += 1;
                    posInd = posInd % 4;



                } while( c == k || a + k < 0 || a + k >= width || b + c < 0 || b + c >= width);

                if (board [a + k, b + c] != TileType.Path) {
                    a = a + k;
                    b = b + c;
                    if (checkNeighboars(a, b)) {
                        if (board [a, b] != TileType.StartPoint) {
                            board [a, b] = TileType.Path;
                        }
                        tiles[a,b].GetComponent<TileBehaviour> ().type = (int)board [a, b];
                        path.Push (new Vector2(a, b));
                        Debug.Log ("X " + a);
                        Debug.Log ("Y " + b);

                    }
                    //tiles [a, b].transform.Translate (Vector3.up);

                }
                Vector2 lastStep = checkFinalPoint (a, b);
                a = (int)lastStep.x;
                b = (int)lastStep.y;



            }

            foreach (Vector2 v in path) {
                Debug.Log (v);
                Debug.Log (board [(int)v.x, (int)v.y]);
                tiles [(int)v.x, (int)v.y].transform.Translate (Vector3.down);
            }


        }

        public Vector2 checkFinalPoint(int i, int j){
            if (path.Count > 1) {
                if (Vector2.Distance (FinishPoint, (Vector2)path.Peek ()) == 1 && Mathf.Abs(i) != Mathf.Abs(j)) {
                    path.Push (FinishPoint);
                    tiles [(int)FinishPoint.x, (int)FinishPoint.y].transform.Translate (Vector3.up);
                    return FinishPoint;
                }
            }
            return new Vector2 (i, j);

        }


        public bool checkNeighboars(int i, int j){
            List<bool> posDirs = new List<bool>();


            if (i >= 0 && i < width && j >= 0 && j < height) {

                posDirs.Add(j + 1 < height && (int)board [i, j + 1] > 1);//up
                posDirs.Add(j - 1 >= 0 && (int)board [i, j - 1] > 1);//down
                posDirs.Add(i - 1 >= 0 && (int)board [i - 1, j] > 1);//left
                posDirs.Add(i + 1 < width && (int)board [i + 1, j] > 1);//right


                return posDirs.Where(e => e == true).Count() > 2;

            } else {
                return false;
            }




        }
        */
}
