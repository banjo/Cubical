﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{

    public List<AudioClip> levelMusicChangeArray;
    public AudioSource audioSource;
    static int lastScene;

    private static SoundManager instance = null;
    public static SoundManager Instance { get { return instance; } }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return; 							//Avoid doing anything else
        }
        else
        {
            instance = this;
            audioSource = GetComponent<AudioSource>();
            audioSource.volume = PlayerPrefsManager.GetMusicVolume();
            audioSource.loop = true;
            audioSource.Play();
            SceneManager.sceneLoaded += SceneLoaded;


            DontDestroyOnLoad(this.gameObject);
        }

    }


    void SceneLoaded(Scene scene, LoadSceneMode m)
    {

        if (levelMusicChangeArray[scene.buildIndex] != null && scene.buildIndex != lastScene)
        {
            lastScene = SceneManager.GetActiveScene().buildIndex;
            audioSource.Stop();
            audioSource.clip = levelMusicChangeArray[scene.buildIndex];
            audioSource.loop = true;
            audioSource.Play();

        }
        else if (lastScene == 3 && lastScene != scene.buildIndex)
        {
            lastScene = SceneManager.GetActiveScene().buildIndex;
            audioSource.Stop();
            audioSource.clip = levelMusicChangeArray[0];
            audioSource.loop = true;
            audioSource.Play();
        }

    }

    public void SetMusicVolume(float volume)
    {
        audioSource.volume = volume;
    }
}
