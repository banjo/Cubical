﻿using UnityEngine;
public class MainController : MonoBehaviour
{

    public GameObject Tile;

    public GameObject Character;



    Transform tileTransform;
    Vector3 angle;
    Vector3 initRot;
    Vector3 lerpEnd;
    MenuStates CurrentState;


    enum MenuStates
    {
        Play = 0,
		Profile,
		Levels,
		Store
    }

    void Start() {
        initRot = Tile.transform.eulerAngles;
        tileTransform = Tile.transform;
        angle = Vector3.zero;
        lerpEnd = new Vector3(0f, 1f, 0f);
    }

    void Update()
    {
        ChangeStates();



    }

	private void ChangeStates(){
        if (SwipeControls.Instance.Direction == SwipeDirection.Left)
        {
            angle = new Vector3(0f, 90f, 0f);
            //CurrentState = 

        }
        else if (SwipeControls.Instance.Direction == SwipeDirection.Right)
        {
            angle = new Vector3(0f, -90f, 0f);
            //initRot = initRot + new Vector3(0, 360f, 0);
        }

        Vector3 to = initRot + angle;
		if(to.y > 360)
            to = new Vector3(to.x, to.y%360, to.z);
		else if(to.y < 0)
            to = new Vector3(to.x, to.y + 360f, to.z);

        //if (tileTransform.rotation.eulerAngles != to || SwipeControls.Instance.Direction != SwipeDirection.None) {
			if (tileTransform.eulerAngles.y <= to.y - lerpEnd.y || tileTransform.eulerAngles.y >= to.y + lerpEnd.y)
				tileTransform.rotation = Quaternion.Lerp(Tile.transform.rotation, Quaternion.Euler(to), Time.deltaTime*15);
			else
			{
                to = new Vector3(Mathf.Round(to.x), Mathf.Round(to.y), Mathf.Round(to.z));
                tileTransform.eulerAngles = to;
                initRot = tileTransform.eulerAngles;
				angle = Vector3.zero;
            }
		//}else {
          //  angle = Vector3.zero;
        //}
	}
}
