﻿using UnityEngine;

public class CameraController : MonoBehaviour
{

    Animator UIAnimator;

    public static bool trackPlayer = false;
    Transform playerTransform;

    bool bitch = false;
    float ScreenHeight;

    // Use this for initialization
    void Start()
    {

        GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
        ScreenHeight = canvas.GetComponent<RectTransform>().rect.height;
        UIAnimator = canvas.GetComponent<Animator>();

        playerTransform = Player.Instance.transform;
        /*
     	foreach(AnimationClip ac in UIAnimator.runtimeAnimatorController.animationClips)
		{
			switch (ac.name)
			{
				case "ShowSlide":
					ac.SetCurve("Main Camera",
								typeof(Camera),
								"m_NormalizedViewPortRect.y",
								AnimationCurve.Linear(0, 0, 0.5f, 1/(ScreenHeight/100)));
					break;
				case "Freeze":
					ac.SetCurve("Main Camera",
								typeof(Camera),
								"m_NormalizedViewPortRect.y",
								new AnimationCurve(new Keyframe(0, 1/(ScreenHeight/100))));
					break;
				case "HideSlide":
					ac.SetCurve("Main Camera",
								typeof(Camera),
								"m_NormalizedViewPortRect.y",
								AnimationCurve.Linear(0, 1/(ScreenHeight/100), 0.5f, 0));
					break;
			};
			
		}
		*/
    }


    // Update is called once per frame
    void Update()
    {


        Vector3 p = playerTransform.position;

        if (trackPlayer)
        {
            Vector3 targetPos = new Vector3(p.x, p.y + 8, p.z - 3);
            transform.position = Vector3.Lerp(transform.position, targetPos, 3.0f * Time.deltaTime);
            if (transform.position == targetPos)
            {
                PlayerBehaviour.CanPlay = true;
            }
        }
    }
}
