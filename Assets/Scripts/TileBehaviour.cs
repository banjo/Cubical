﻿using UnityEngine;

public class TileBehaviour : MonoBehaviour
{

    //MeshRenderer mr;
    public Material nextColor;
    public GameObject player;
    public Board.TileType type;
    public bool visited;
    // Event Delegate
    public delegate void TileHandler(Board.TileType type);
    public static event TileHandler playerMoved;

    // Use this for initialization
    void Start()
    {
        //mr = GetComponent<MeshRenderer>();
    }
    /*
	void FixedUpdate(){
		if (transform.position.y < 0) {
			transform.Translate (new Vector3 (0, 0.02f, 0));
		} else if (transform.position.y > 0) {
            mr.material = nextColor;
            transform.position = new Vector3(transform.position.x, 0, transform.position.z);
		}
	}
	*/

    void OnCollisionEnter(Collision collision)
    {
        if (playerMoved != null)
        {
            playerMoved(type);
        }

    }


    /*
	public void OnMouseOver()
	{
		if (Input.GetMouseButtonDown (0)) {
			Debug.Log ("CLICLED");

			CubeBehaviour cb = player.GetComponent<CubeBehaviour> ();

			cb.padPos = transform.position;

			cb.autoMove = true;

			int padX = Mathf.RoundToInt(transform.position.x);
			int padZ = Mathf.RoundToInt(transform.position.z);
			int plyX = Mathf.RoundToInt(player.transform.position.x);
			int plyZ = Mathf.RoundToInt(player.transform.position.z);

			if (padX == plyX) {
				if (padZ < plyZ) {
					cb.directionZ = -1;
				} else if (padZ > plyZ) {
					cb.directionZ = 1;
				} else {
					//autoMove = false;
					cb.directionZ = 0;
				}
			} else if (padZ == plyZ && cb.directionX == 0f) {
				if (padX < plyX) {
					cb.directionX = 1;
				} else if (padX > plyX) {
					cb.directionX = -1;
				} else {
					//autoMove = false;
					cb.directionX = 0;
				}
			}

			player.transform.position = new Vector3((float)plyX, player.transform.position.y, (float)plyZ);

			Debug.Log ("DIRX " + cb.directionX);
			Debug.Log ("DIRZ " + cb.directionZ);
			*/



}
