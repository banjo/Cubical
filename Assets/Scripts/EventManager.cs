﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class EventManager : MonoBehaviour
{
    public enum SceneState
    {
        Main = 0,
        Levels,
        Options,
        Game,
        Profile,
        GameOver
    }

    public void LoadScene(int state)
    {
        SceneManager.LoadScene(Enum.GetName(typeof(SceneState), state));
    }

    public static void Load_Scene(int state)
    {
        SceneManager.LoadScene(Enum.GetName(typeof(SceneState), state));
    }

    public void LoadLevel(int i)
    {
        PlayerPrefsManager.SetCurrentLevelIndex(i - 1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    public void ReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        PlayerBehaviour.checkPos = Vector3.zero;
    }

    public void LoadNextLevel()
    {
        int cli = Game.Instance.CurrentLevelInd++;
        PlayerPrefsManager.SetCurrentLevelIndex(cli + 1);
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        ReloadLevel();
    }

    public void PauseGame()
    {
        PlayerBehaviour.CanPlay = false;
    }


    public void ResumeGame()
    {
        PlayerBehaviour.CanPlay = true;
    }


    public void FreezeTime()
    {
        Time.timeScale = 0.0f;
    }

    public void UnFreezeTime()
    {
        Time.timeScale = 1.0f;
    }

    public void SlowDownTime(float t)
    {
        Time.timeScale = t;
    }

    public void SpeedUpTime(float t)
    {
        Time.timeScale = t;
    }

    public void canLook()
    {
        CameraLook.can = true;
    }
}

