﻿using UnityEngine;

public class CameraLook : MonoBehaviour
{

    private Vector3 firstpoint; //change type on Vector3
    private Vector3 secondpoint;
    private float xAngle; //angle for axes x for rotation
    private float yAngle;
    private float xAngTemp; //temp variable for angle
    private float yAngTemp;

    private float VertGap;
    private float HorZGap;

    public static bool can = false;

    // Use this for initialization
    void Start()
    {

        //Initialization our angles of camera
        xAngle = 0f;
        yAngle = 0f;
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            //Touch began, save position
            if (Input.GetTouch(0).phase == TouchPhase.Began && can)
            {
                firstpoint = Input.GetTouch(0).position;
                xAngTemp = xAngle;
                yAngTemp = yAngle;
            }
            //Move finger by screen
            else if (Input.GetTouch(0).phase == TouchPhase.Moved)
            {
                secondpoint = Input.GetTouch(0).position;
                //Mainly, about rotate camera. For example, for Screen.width rotate on 180 degree
                xAngle = xAngTemp = (secondpoint.y - firstpoint.y) * -90 / Screen.width;
                yAngle = yAngTemp - (secondpoint.x - firstpoint.x) * -180 / Screen.height;
                //Rotate camera

                if (Mathf.Abs(xAngle) < 20 && Mathf.Abs(yAngle) < 20)
                {
                    Debug.Log("X: " + xAngle.ToString());
                    Debug.Log("Y: " + yAngle.ToString());
                    this.transform.rotation = Quaternion.Euler(xAngle / 2, 0, yAngle / 2);
                }
            }
        }
        else
        {
            this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(0, 0, 0), Time.deltaTime * 5);
            xAngle = 0f;
            yAngle = 0f;
        }

    }
}
