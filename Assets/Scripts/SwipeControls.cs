﻿using UnityEngine;

public enum SwipeDirection
{
    None = 0,   // 0000
    Left = 1,   // 0001
    Right = 2,  // 0010
    Up = 4,     // 0100
    Down = 8    // 1000
}

public class SwipeControls : MonoBehaviour
{

    private static SwipeControls instance;

    public static SwipeControls Instance { get { return instance; } }

    public SwipeDirection Direction { get; set; }
    private Vector2 touchPosition;
    private float swipeResistanceX = 20.0f;
    private float swipeResistanceY = 20.0f;


    // Use this for initialization
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        Direction = SwipeDirection.None;

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            touchPosition = Input.GetTouch(0).position;
        }

        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
        {

            Vector2 deltaSwipe = touchPosition - Input.GetTouch(0).position;
            float angle = Vector2.Angle(deltaSwipe, Vector2.right);
            if (Mathf.Abs(deltaSwipe.x) > swipeResistanceX && angle < 45 || angle > 125)
            {
                // Swipe on the x axis
                Direction |= (deltaSwipe.x < 0) ? SwipeDirection.Right : SwipeDirection.Left;
            }
            else if (Mathf.Abs(deltaSwipe.y) > swipeResistanceY && angle > 45 && angle < 125)
            {
                // Swipe on the y axis
                Direction |= (deltaSwipe.y < 0) ? SwipeDirection.Up : SwipeDirection.Down;
            }

        }
    }

    public bool IsSwiping(SwipeDirection dir)
    {
        return (Direction & dir) == dir;
    }
}
