﻿using UnityEngine;

// Singleton Player
public class Player : MonoBehaviour
{

    // Game Stats and Scores
    public int PlayerGold;      // Gold that can be used in store and for other purchases
    public int PlayerGems;      // Rare Stones that can has great value
    public int Keys;            // Keys for level tries replenishes with time
    public PlayerBehaviour playerBehaviour;
    // public GameObject txunula;
    // public int txunulaLifeJars;

    // Player Singleton Instance
    private static Player instance = null;
    public static Player Instance { get { return instance; } }

    private void Awake()
    {
        //Keys //TODO //= PlayerPrefs.GetInt("jars");
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;//Avoid doing anything else
        }
        instance = this;
    }
}
