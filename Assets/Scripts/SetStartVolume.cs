﻿using UnityEngine;

public class SetStartVolume : MonoBehaviour
{

    private SoundManager musicManager;

    void Start()
    {
        musicManager = GameObject.FindObjectOfType<SoundManager>();
        if (musicManager)
        {
            float volume = PlayerPrefsManager.GetMusicVolume();
            musicManager.SetMusicVolume(volume);
        }
    }

}
