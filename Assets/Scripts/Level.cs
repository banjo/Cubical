﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Level
{

    public Board board;

    public Vector2 StartPoint;
    public Vector2 FinishPoint;

    public List<Vector3> walked;

    public int Combo;
    public int LevelGold;
    public int LevelGems;
    public int LevelScore;
    public int TrapCount;
    public int GoldPerTile = 2;

    public float LevelTime;
    public float timeUntilComboEnds = 0.0f;
    public float timeForNextCombo = 1.0f;

    public Material pathMaterial;


    // LevelDestroy event
    public delegate void OnLevelFinishHandler();
    public event OnLevelFinishHandler levelFinished;

    // Add Gold Event
    public delegate void OnTileGoldHandler(int currentGold);
    public event OnTileGoldHandler tileGold;

    // Trapped
    public delegate void OnPlayerTrappedHandler(Board.TileType type);
    public event OnPlayerTrappedHandler failLevel;

    // Combo event
    public delegate void OnComboChangedHandler(int currentCombo, float comboTime);
    public event OnComboChangedHandler comboChange;

    // Full Level Score
    public delegate void OnLevelGoldChanged(int tileGold, int LevelGold, int LevelScore);
    public event OnLevelGoldChanged statsChange;

    //public TextMesh 


    // Load Level
    public Level(int[][] boardMatrix, GameObject tile, GameObject chest, Material pathMaterial)
    {
        this.pathMaterial = pathMaterial;
        board = new Board(boardMatrix, tile, chest);
        StartPoint = board.StartPoint;
        FinishPoint = board.FinishPoint;
        LevelManager.Instance.SpawnPoint = new Vector3(StartPoint.x, 0, StartPoint.y);
        //Player.Instance.playerBehaviour.transform.position = new Vector3(StartPoint.x, 2f, StartPoint.y);
        TileBehaviour.playerMoved += playerMoved;
        Combo = 0;
        LevelGold = 0;
        LevelGems = 0;
        LevelScore = 0;
        LevelTime = 10f;//TODO it's default now
        TrapCount = 0;
        walked = new List<Vector3>();
    }

    public void Update()
    {
        if (timeUntilComboEnds < Time.time && timeUntilComboEnds != 0.0f)
        {
            timeUntilComboEnds = 0.0f;
            timeForNextCombo = 1.0f;
            Combo = 0;
            comboChange(Combo, 0);

        }
        if (comboChange != null && timeUntilComboEnds != 0.0f)
        {
            float tim = Mathf.Lerp(0, 1, timeUntilComboEnds - Time.time);
            comboChange(Combo, tim);
        }
    }

    ~Level()
    {
        TileBehaviour.playerMoved -= playerMoved;
        board = null;
        // tileGold -= tileScore;
        // comboChange -= comboChange;
        // goldChange -= goldChange;
    }

    void playerMoved(Board.TileType type)
    {
        Vector3 currentPosition = RoundVector(Player.Instance.transform.position); // current position of player
        Vector3 checkPos = RoundVector(PlayerBehaviour.checkPos);
        List<Vector3> tileVects = Board.path.Select(z => z.transform.position).ToList(); // get tile vectors from path
        switch (type)
        {
            case Board.TileType.StartPoint:
                PlayerBehaviour.CanPlay = true;
                break;
            case Board.TileType.CheckPoint:
                //TODO
                PlayerBehaviour.CanPlay = true;
                if (checkPos != currentPosition || PlayerBehaviour.SecondChance)
                {
                    PlayerBehaviour.checkPos = Player.Instance.transform.position;
                    PlayerBehaviour.SecondChance = true;
                }
                else
                {
                    PlayerBehaviour.SecondChance = false;
                }
                break;
            //goto case Board.TileType.Path;
            case Board.TileType.Path:
                if (timeUntilComboEnds == 0.0f || timeUntilComboEnds > Time.time)
                {
                    timeUntilComboEnds = Time.time + timeForNextCombo;
                    Combo++;
                    timeForNextCombo -= 0.1f;
                }
                if (!walked.Contains(currentPosition))
                {
                    LevelScore += (Combo * GoldPerTile);
                    LevelGold += GoldPerTile; //TODO
                    //if (comboChange != null)
                    //    comboChange(Combo, Time.time/timeUntilComboEnds);
                    if (statsChange != null)
                        statsChange(GoldPerTile, LevelGold, LevelScore);
                }
                break;
            case Board.TileType.FinishPoint:
                if (levelFinished != null)
                {
                    levelFinished();
                    TileBehaviour.playerMoved -= playerMoved;
                }
                break;
            default:
                Vector3 p = checkPos;
                if (p != Vector3.zero && PlayerBehaviour.SecondChance)
                {
                    int i = 0;
                    Player.Instance.gameObject.SetActive(false);
                    while (walked[i] != RoundVector(checkPos))
                    {
                        Board.path[tileVects.IndexOf(walked[i])].GetComponent<TileBehaviour>().visited = true;
                        i++;
                    }

                    PlayerBehaviour.SecondChance = false;

                    LevelManager.Instance.buzzLighter = true;
                    PlayerBehaviour.CanPlay = false;
                    Player.Instance.playerBehaviour.isRotate = false;
                    LevelManager.Instance.SpawnPoint = checkPos;
                    //Player.Instance.playerBehaviour.transform.position = new Vector3(p.x, 2f, p.z);
                    //Camera.main.transform.parent.position = new Vector3((LevelManager.width / 2.0f) - .5f, Mathf.Max(LevelManager.height, LevelManager.width) * 2f, -2);
                    Camera.main.transform.localPosition = Vector3.zero;
                    //Camera.main.transform.LookAt(new Vector3((LevelManager.width / 2.0f) - .5f, 0, (LevelManager.height / 2.0f) + .5f));

                }
                else if (failLevel != null)
                {
                    failLevel(type);
                    TileBehaviour.playerMoved -= playerMoved;
                }
                return;
        }
        walked.Add(currentPosition);
        Board.path[tileVects.IndexOf(currentPosition)].GetComponent<MeshRenderer>().material = pathMaterial;
    }


    Vector3 RoundVector(Vector3 a)
    {
        return new Vector3(Mathf.Round(a.x), Mathf.Round(a.y), Mathf.Round(a.z));

    }

}
