﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Text.RegularExpressions;
using System;

public class LevelEvent : MonoBehaviour, IPointerClickHandler
{

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Game.Instance.Keys >= 0)
        {
            int i = Int32.Parse(Regex.Match(gameObject.name, @"\d+").Value);
            PlayerPrefsManager.SetCurrentLevelIndex(i - 1);
            Debug.Log("CURRRRR" + PlayerPrefsManager.GetCurrentLevelIndex());
            Game.Instance.CurrentLevelInd = i - 1;
            Vector3 s = GameObject.FindGameObjectWithTag("Scrollable").GetComponent<RectTransform>().localPosition;
            PlayerPrefsManager.SetScrollViewPosition(s.y);
            EventManager.Load_Scene(3);
        }
    }

}
