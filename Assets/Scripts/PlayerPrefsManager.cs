﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerPrefsManager : MonoBehaviour
{

    const string MUSIC_VOLUME_KEY = "music_volume";
    const string SOUND_VOLUME_KEY = "sound_volume";
    const string UNLOCKED_LEVEL_KEY = "level_unlocked_";
    const string CURRENT_LEVEL_KEY = "level_index";
    const string SCROLLVIEW_POSITION = "scrollview_position";
    const string HEALTH_JARS_KEY = "jars";
    const string KEYS_KEY = "keys";
    const string COINS_KEY = "coins";
    const string GEMS_KEY = "gems";

    // CURRENT LEVEL INDEX

    //GET
    public static int GetCurrentLevelIndex()
    {
        return PlayerPrefs.GetInt(CURRENT_LEVEL_KEY);
    }
    //SET
    public static void SetCurrentLevelIndex(int ind)
    {
        PlayerPrefs.SetInt(CURRENT_LEVEL_KEY, ind);
    }


    // JARS

    //GET
    public static int GetJars()
    {
        return PlayerPrefs.GetInt(HEALTH_JARS_KEY);
    }
    //SET
    public static void SetJars(int jar)
    {
        PlayerPrefs.SetInt(HEALTH_JARS_KEY, jar);
    }


    // KEYS

    //GET
    public static int GetKeys()
    {
        return PlayerPrefs.GetInt(KEYS_KEY);
    }
    //SET
    public static void SetKeys(int k)
    {
        PlayerPrefs.SetInt(KEYS_KEY, k);
    }


    // GEMS

    //GET
    public static int GetGems()
    {
        return PlayerPrefs.GetInt(GEMS_KEY);
    }
    //SET
    public static void SetGems(int k)
    {
        PlayerPrefs.SetInt(GEMS_KEY, k);
    }


    // COIN

    //GET
    public static int GetCoins()
    {
        return PlayerPrefs.GetInt(COINS_KEY);
    }
    //SET
    public static void SetCoins(int k)
    {
        PlayerPrefs.SetInt(COINS_KEY, k);
    }


    // SCROLL VIEW POSITION Y

    //GET
    public static float GetScrollViewPosition()
    {
        return PlayerPrefs.GetFloat(CURRENT_LEVEL_KEY);
    }
    //SET
    public static void SetScrollViewPosition(float ind)
    {
        PlayerPrefs.SetFloat(CURRENT_LEVEL_KEY, ind);
    }


    // MUSIC VOLUME

    //GET
    public static float GetMusicVolume()
    {
        return PlayerPrefs.GetFloat(MUSIC_VOLUME_KEY);
    }
    //SET
    public static void SetMusicVolume(float volume)
    {
        if (volume >= 0f && volume <= 1f)
        {
            PlayerPrefs.SetFloat(MUSIC_VOLUME_KEY, volume);
        }
        else
        {
            Debug.LogError("Music Volume out of range");
        }
    }

    // SOUND VOLUME

    //GET
    public static float GetSoundVolume()
    {
        return PlayerPrefs.GetFloat(SOUND_VOLUME_KEY);
    }
    //SET
    public static void SetSoundVolume(float volume)
    {
        if (volume >= 0f && volume <= 1f)
        {
            PlayerPrefs.SetFloat(SOUND_VOLUME_KEY, volume);
        }
        else
        {
            Debug.LogError("Sound Volume out of range");
        }
    }


    //TODO UNLOCK LEVEL
    public static void UnlockLevel(int level)
    {
        if (level <= SceneManager.sceneCountInBuildSettings - 1)
        {
            PlayerPrefs.SetInt(UNLOCKED_LEVEL_KEY + level.ToString(), 1);
        }
        else
        {
            Debug.LogError("trying unlock level not in build order");
        }
    }


    //TODO CHECK IF LEVEL IS UNLOCKED
    public static bool IsLevelUnlocked(int level)
    {
        int levelValue = PlayerPrefs.GetInt(UNLOCKED_LEVEL_KEY + level.ToString());
        bool IsLevelUnlocked = (levelValue == 1);

        if (level <= SceneManager.sceneCountInBuildSettings - 1)
        {
            return IsLevelUnlocked;
        }
        else
        {
            Debug.LogError("trying query level not in build order");
            return false;
        }
    }

}
