﻿using UnityEngine;
using UnityEngine.UI;

public class Profile : MonoBehaviour
{

    public Text CurrentLevelText;

    public Text CoinText;
    public Text GemText;
    public Text KeyText;

    void Awake()
    {

        CurrentLevelText.text = "Level " + (Game.Instance.CurrentLevelInd + 1).ToString();

        CoinText.text = PlayerPrefsManager.GetCoins().ToString();
        GemText.text = PlayerPrefsManager.GetGems().ToString();
        KeyText.text = PlayerPrefsManager.GetKeys().ToString();

    }

}
