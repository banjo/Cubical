﻿using System;
using UnityEngine;
using System.Collections.Generic;

// Game Singleton
public class Game : MonoBehaviour
{

    public int CurrentLevelInd;
    public int[][][] LevelStates;

    public int Keys; // khaled keys
    public int Gems;
    public int Coins;

    // Player Singleton Instance
    private static Game instance = null;
    public static Game Instance { get { return instance; } }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;//Avoid doing anything else
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
    }


    // Use this for initialization
    void Start()
    {


        //Screen.orientation = ScreenOrientation.Portrait;
        int k = PlayerPrefsManager.GetKeys();
        int g = PlayerPrefsManager.GetGems();
        int c = PlayerPrefsManager.GetCoins();
        Debug.Log("DONE KEY" + k);
        if (k == 0)
        {
            PlayerPrefsManager.SetKeys(10);
            Keys = 10;
            Debug.Log("DONE KEY");
        }

        if (g == 0)
        {
            PlayerPrefsManager.SetGems(5);
            Gems = 5;
        }

        if (c == 0)
        {
            PlayerPrefsManager.SetCoins(100);
            Coins = 100;
        }

        ImportLevelsFromFile("Levels");
        CurrentLevelInd = PlayerPrefsManager.GetCurrentLevelIndex();
        // if(CurrentLevelInd == 0){
        //     CurrentLevelInd = 1;
        // }


        //Vector3 groundSize = ground.GetComponent<Collider>().bounds.size;
    }


    void OnLevelWasLoaded(int level)
    {

        GameObject scrollView = GameObject.FindGameObjectWithTag("Scrollable");
        if (scrollView != null)
        {
            scrollView.GetComponent<RectTransform>().localPosition = new Vector3(0, PlayerPrefsManager.GetScrollViewPosition(), 0);
        }

    }


    public void ImportLevelsFromFile(string path)
    {

        List<int[][]> levels = new List<int[][]>();
        int levelInd = -1;

        //#if UNITY_ANDROID
        TextAsset txt = Resources.Load(path) as TextAsset;
        string[] lines = txt.text.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

        string item;
        for (int i = 0; i < lines.Length; i++)
        {
            item = lines[i];

            if (item.Contains("="))
            {
                levelInd++;
                item = lines[++i];
                int[] dims = Array.ConvertAll(item.Split('x'), k => int.Parse(k));
                int[][] matrix = new int[dims[1]][];

                for (int j = 0; j < dims[1]; j++)
                {
                    matrix[j] = Array.ConvertAll(lines[++i].Split(','), k => int.Parse(k));
                }
                levels.Add(matrix);

            }

        }
        LevelStates = levels.ToArray();

        //#endif

        /*
        #if UNITY_STANDALONE || UNITY_WEBGL

                FileStream fs = new FileStream("Assets/Resources/" + path + ".txt", FileMode.Open);
                StreamReader sr = new StreamReader(fs);

                string line = "";
                while (!sr.EndOfStream) {
                    line = sr.ReadLine();

                    if (line.Contains("=")) {
                        levelInd++;
                        line = sr.ReadLine();
                        int[] dims = Array.ConvertAll(line.Split('x'), i => int.Parse(i));
                        int[][] matrix = new int[dims[1]][];

                        for (int i = 0; i < dims[1]; i++) {
                            matrix[i] = Array.ConvertAll(sr.ReadLine().Split(','), k => int.Parse(k));
                        }
                        levels.Add(matrix);

                    }
                }

                LevelStates = levels.ToArray();

                sr.Close();
                fs.Close();
        #endif
        */

    }
}
