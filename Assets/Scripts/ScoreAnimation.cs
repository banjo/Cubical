﻿using UnityEngine;

public class ScoreAnimation : MonoBehaviour
{

    public float duration = 5.0f;
    public string text;
    Vector3 ForcePoint;
    Vector3 ChestPos;
    TextMesh mesh;
    Rigidbody rigidBody;
    bool destroyed;

    void Start()
    {
        GameObject chest = GameObject.FindGameObjectWithTag("Chest");

        ChestPos = chest != null ? chest.transform.position : this.transform.position;

        ForcePoint = ChestPos + Vector3.up * 2;
        //ForcePoint = new Vector3(10f,0f,10f);
        mesh = transform.GetChild(0).GetComponent<TextMesh>();
        mesh.text = text;

    }

    void Update()
    {
        if (Vector3.Distance(transform.position, ForcePoint) > 1f)
            transform.Translate((ForcePoint - transform.position).normalized / Random.Range(5, 10));// = Vector3.Lerp(transform.position, ForcePoint, Time.deltaTime);
        else if (Vector3.Distance(transform.position, ChestPos) < 1f)
            Destroy(this.gameObject);
        else
            ForcePoint = ChestPos;
    }

    void OnTriggerEnter(Collider col)
    {
        ScoreAnimation coin = col.gameObject.GetComponent<ScoreAnimation>();
        if (coin != null && !destroyed)
        {
            mesh.text = "+" + (int.Parse(this.text.Substring(1)) + int.Parse(coin.mesh.text.Substring(1))).ToString();
            coin.destroyed = true;
            Destroy(coin.gameObject);
        }
    }

}
