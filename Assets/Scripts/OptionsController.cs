﻿using UnityEngine;
using UnityEngine.UI;

public class OptionsController : MonoBehaviour
{
    public Slider SoundSlider;
    public Slider MusicSlider;

    private SoundManager soundManager;
    private float soundVolume;

    void Start()
    {
        soundManager = GameObject.FindObjectOfType<SoundManager>();
        SoundSlider.value = PlayerPrefsManager.GetSoundVolume();
        MusicSlider.value = PlayerPrefsManager.GetMusicVolume();
    }

    void Update()
    {
        soundVolume = SoundSlider.value;
        soundManager.SetMusicVolume(MusicSlider.value);
    }

    public void SaveAndExit()
    {
        PlayerPrefsManager.SetSoundVolume(soundVolume);
        PlayerPrefsManager.SetMusicVolume(MusicSlider.value);
        EventManager.Load_Scene(0);
    }

    public void SetDefault()
    {
        SoundSlider.value = 1f;
        MusicSlider.value = 1f;
    }
}
