﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{

    public int width;
    public int height;
    public bool buzzLighter; // Path Color Changer
    public Vector3 SpawnPoint;

    public int HealthJars;      // Jars for level tries replenishes with time

    public float pathShowTime = 0.0f;

    public Level CurrentLevel;

    public Material oldColor;
    public Material newColor;

    public GameObject gameCamera;

    //Prefab
    public GameObject PlayerObject;
    public GameObject TilePrefab;
    public GameObject CoinPrefab;
    public GameObject ChestPrefab;

    public GameObject ComboObject;
    public Text GoldText;
    public Text TitleText;
    public Text ScoreText;

    public GameObject ground;

    [System.SerializableAttribute]
    public struct Sound
    {
        public Board.TileType type;
        public AudioClip audio;
    }

    public Sound[] sounds;


    // private
    Game game = Game.Instance;
    float timeUntilStart = 0.0f;
    Animator UIAnimator;
    AudioSource audioSource;

    int CurrentLevelInd;


    // LevelManager Singleton Instance
    private static LevelManager instance = null;
    public static LevelManager Instance { get { return instance; } }

    void Awake()
    {

        //Keys //TODO //= PlayerPrefs.GetInt("jars");
        // if the singleton hasn't been initialized yet
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;//Avoid doing anything else
        }
        instance = this;

        if (!SoundManager.Instance.audioSource.isPlaying)
            SoundManager.Instance.audioSource.Play();


        CurrentLevelInd = Game.Instance.CurrentLevelInd;

        width = game.LevelStates[CurrentLevelInd][0].Length;
        height = game.LevelStates[CurrentLevelInd].Length;

        TitleText.text = "Level " + (CurrentLevelInd + 1);

        // Create Level
        CurrentLevel = new Level(game.LevelStates[CurrentLevelInd], TilePrefab, ChestPrefab, newColor);
        // Add Event Listeners
        CurrentLevel.levelFinished += FinishLevel;
        CurrentLevel.failLevel += playerTrapped;
        CurrentLevel.comboChange += comboChange;
        CurrentLevel.statsChange += statsChange;

        float max = Mathf.Max(height, width);

        gameCamera.transform.position = new Vector3((width / 2.0f) - .5f, max * 2f, -2);
        gameCamera.transform.GetChild(0).LookAt(new Vector3((width / 2.0f) - .5f, 0, (height / 2.0f) + .5f));

        //ground = transform.FindChild("Plane").gameObject;

        ground.transform.Translate(width / 2, 0f, height / 2);
        ground.transform.localScale = new Vector3(width / 5, 0f, height / 20);

        // Reset Camera And Player State
        CameraController.trackPlayer = false;
        PlayerBehaviour.CanPlay = false;

        // Set Canvas Animator
        UIAnimator = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Animator>();

        audioSource = GetComponent<AudioSource>();
        audioSource.volume = PlayerPrefsManager.GetSoundVolume();

        if (max <= 5)
            pathShowTime = 0.25f;
        else
            pathShowTime = width / 10.0f;


        if (Time.timeScale == 0.0f)
        {
            Time.timeScale = 1f;
        }

        // Set time until start
        if (timeUntilStart == 0.0f)
        {
            buzzLighter = true;
            ChangePathColor(newColor);
            timeUntilStart = Time.time + pathShowTime;
        }
    }

    void OnDestroy()
    {
        if (CurrentLevel != null)
        {
            CurrentLevel.comboChange -= comboChange;
            CurrentLevel.statsChange -= statsChange;
            CurrentLevel.levelFinished -= FinishLevel;
            CurrentLevel.failLevel -= playerTrapped;

        }

    }

    void statsChange(int tileGold, int levelGold, int levelScore)
    {
        GoldText.text = "Gold: " + levelGold.ToString();
        ScoreText.text = "Score: " + levelScore.ToString();
        PlaySound((int)Board.TileType.Path);
        GameObject coinObject = Instantiate(CoinPrefab, Player.Instance.transform.position + Vector3.up, Quaternion.Euler(45f, 0, 0)) as GameObject;
        coinObject.GetComponent<ScoreAnimation>().text = "+" + tileGold.ToString();
    }

    void comboChange(int combo, float comboTime)
    {
        ComboObject.GetComponent<Image>().fillAmount = comboTime;
        ComboObject.GetComponentInChildren<Text>().text = "x" + combo.ToString();

    }


    void playerTrapped(Board.TileType type)
    {
        SoundManager.Instance.audioSource.Stop();
        PlaySound((int)type);
        Debug.Log("Player Trapped!");
        PlayerBehaviour.CanPlay = false;
        UIAnimator.SetTrigger("FailPanelOn");

    }

    void FinishLevel()
    {
        // FINISH current level add gold and display stats
        PlayerBehaviour.CanPlay = false;
        //LevelManager.CurrentLevelInd++;
        //CurrentLevel = null;
        SoundManager.Instance.audioSource.Stop();
        UIAnimator.SetTrigger("FinishPanelOn");
        PlaySound((int)Board.TileType.FinishPoint);
        //SceneManager.LoadScene("_Scenes/Levels");
    }

    void Update()
    {
        if (buzzLighter)
        {
            timeUntilStart = Time.time + pathShowTime + CurrentLevelInd / 4;
            ChangePathColor(newColor);
            buzzLighter = false;
            CameraController.trackPlayer = false;
        }

        if (timeUntilStart < Time.time && timeUntilStart > 0)
        {
            timeUntilStart = -1f;
            ChangePathColor(oldColor);
            PlayerObject.SetActive(true);
            PlayerObject.transform.position = new Vector3(SpawnPoint.x, SpawnPoint.y + 2f, SpawnPoint.z);
            //PlayerBehaviour.CanPlay = true;
            if (width >= 10)
                CameraController.trackPlayer = true;
        }

        if (CurrentLevel != null)
        {
            CurrentLevel.Update();
        }
    }

    public void ChangePathColor(Material pathColor)
    {
        List<GameObject> tiles = Board.path;

        for (int i = 0; i < tiles.Count; i++)
        {
            if (!tiles[i].GetComponent<TileBehaviour>().visited)

                tiles[i].GetComponent<MeshRenderer>().material = pathColor;
        }

    }

    public void PlaySound(int ind)
    {
        Sound s = sounds[ind];
        if (s.audio != null)
        {
            audioSource.Stop();
            audioSource.clip = s.audio;
            audioSource.Play();
        }
    }
}
