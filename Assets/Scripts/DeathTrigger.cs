﻿using UnityEngine;

public class DeathTrigger : MonoBehaviour
{

    void OnTriggerExit(Collider col)
    {
        Destroy(col.gameObject);
    }
}
